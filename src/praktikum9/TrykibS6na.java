package praktikum9;

import lib.TextIO;

public class TrykibS6na {

	public static void main(String[] args) {

		System.out.println("Palun sisesta s6na");
		String s6na = TextIO.getlnString();
		System.out.println(t66tlus(s6na));
	}

	private static String t66tlus(String s6na) {
		String t66deldud = "";
		for (int i = 0; i < s6na.length(); i++) {
			if (i > 0) {
				t66deldud += "-";
			}
			
			t66deldud += s6na.toUpperCase().charAt(i);
		//	t66deldud = t66deldud.substring(s6na.length());
		}

		return t66deldud;
	}

}
