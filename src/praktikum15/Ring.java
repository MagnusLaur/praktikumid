package praktikum15;

public class Ring {

	Punkt keskp;
	double raadius;

	public Ring(Punkt p, double r) {
		keskp = p;
		raadius = r;

	}

	@Override
	public String toString() {

		return "Ring(" + keskp + "  " + raadius + ")";

	}

	public double ymberm66t() {
		return 2 * Math.PI * raadius;
	}

	public double pindala() {
		return Math.PI * Math.pow(raadius, 2);
	}
}
