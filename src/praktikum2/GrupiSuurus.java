package praktikum2;

import lib.TextIO;

public class GrupiSuurus {

	public static void main(String[] args) {

		System.out.println("Kui suur on inimeste arv");
		int inimesteArv = TextIO.getlnInt();

		System.out.println("Palun sisesta grupi suuruse arv");
		int grupSuurus = TextIO.getlnInt();

		int gruppideArv = inimesteArv / grupSuurus;
		System.out.println("Saame moodustada " + gruppideArv + " gruppi");
		
		
		// TODO: mitu inimest jääb üle?
		System.out.println("Üle jääb " + inimesteArv % gruppideArv + " inimest");
		
	}

}
