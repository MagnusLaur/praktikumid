package praktikum4;

import lib.TextIO;

public class Valikud {
	public static void main(String[] args) {
		int vastus;

		System.out.println("Kelle taskust täita riigieelarve?");
		System.out.println("1. põllumeeste");
		System.out.println("2. pensionäride");
		System.out.println("3. ettevõtjate");
		System.out.println("4. surnute");

		vastus = TextIO.getlnInt();

		if (vastus == 1) {
			System.out.println("Sa hull tahad vist nälga jääda!");
		} else if (vastus == 2) {
			System.out.println("Tõbras!");
		} else if (vastus == 3) {
			System.out.println("Sa oled vallandatud!");
		} else if (vastus == 4) {
			System.out.println("Isegi surnuid siin ei austata");
		} else {
			System.out.println("Sellist vastusevarianti ei ole!");
		}
	}
}