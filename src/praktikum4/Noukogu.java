package praktikum4;

import lib.TextIO;

public class Noukogu {
	public static void main(String[] args) {
		int poolt;
		int vastu;
		int veto;
		
		System.out.print("Poolt? ");
		poolt = TextIO.getlnInt();
		
		System.out.print("Vastu? ");
		vastu = TextIO.getlnInt();
		
		System.out.print("Veto? ");
		veto = TextIO.getlnInt();
		
		if(poolt > vastu && veto == 0) {
			System.out.println("Korras!");
		}
		else {
			System.out.println("Sorry!");
		}
	}
}
