package praktikum4;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {

		System.out.println("Mis on teie lõputöö hinne?");
		int lõputöö = TextIO.getlnInt();

		System.out.println("Mis on teie keskmine hinne?");
		double keskmine = TextIO.getlnDouble();

		if (lõputöö < 0 || lõputöö > 5) {
			System.out.println("See hinne ei sobi!");
			return;
		}

		if (keskmine < 0 || keskmine > 5) {
			System.out.println("See hinne ei sobi!");
			return;
		}
		// && .. loogine JA
		// || .. loogiline VÕI
		// ! .. eitus

		if (lõputöö == 5 && keskmine > 4.5) {
			System.out.println("Saad Cum Laude!");

		} else {
			System.out.println("Ei saa Cum Laudet!");

		}

	}
}
